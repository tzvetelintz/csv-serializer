#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#ifndef TESTPROGRAM_SERIALIZE_H
#define TESTPROGRAM_SERIALIZE_H

#endif //TESTPROGRAM_SERIALIZE_H

void serialize(char* input_path, char* output_path) ;