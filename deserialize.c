#include <stdio.h>
#include <stdlib.h>

#include "main.c"

void deserialize(char* input_path, char* output_path) {
    FILE *input_file = fopen(input_path, "rb");
    if (input_file == NULL) {
        printf("Error: could not open input file\n");
        exit(EXIT_FAILURE);
    }

    FILE *output_file = fopen(output_path, "w");
    if (output_file == NULL) {
        printf("Error: could not open output file\n");
        exit(EXIT_FAILURE);
    }
    Record record;
//Reads binary files and write csv

    while (fread(&record, sizeof(record), 1, input_file)) {
        fprintf(output_file, FORMAT_INPUT,
                record.row_id,record.order_id, record.order_date, record.customer_id, record.city, record.state, record.postal_code,
                record.region, record.product_id, record.category, record.sub_category, record.price);
    }


//close the files
    fclose(input_file);
    fclose(output_file);
}