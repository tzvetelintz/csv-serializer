#include <stdio.h>
#include <stdlib.h>

#include "main.c"



void serialize(char* input_path, char* output_path) {

    FILE *input_file = fopen(input_path, "r");
    if (input_file == NULL) {
        printf("Error: could not open input file\n");
        exit(EXIT_FAILURE);
    }

    FILE *output_file = fopen(output_path, "wb");
    if (output_file == NULL) {
        printf("Error: could not open output file\n");
        exit(EXIT_FAILURE);
    }

    Record record;
//Reads binary csv and write binary

    int record_count = 0;
    while (fscanf(input_file, FORMAT_OUTPUT,
                  &record.row_id, record.order_id, record.order_date, record.customer_id, record.city, record.state, record.postal_code,
                  record.region, record.product_id, record.category, record.sub_category, &record.price) == 12) {
        fwrite(&record, sizeof(record), 1, output_file);
        record_count++;
    }

    printf("Serialized %d records to file %s\n", record_count, output_path);

//close the files

    fclose(input_file);
    fclose(output_file);
}